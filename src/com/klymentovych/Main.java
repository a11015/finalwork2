package com.klymentovych;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        //outer:

        boolean isQuit = false;
        while (!isQuit) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Hello! Here is menu, you can choose one option by writing only one number");
            System.out.println("1 - Display list of all users");
            System.out.println("2 - Display list of all products");
            System.out.println("3 - Buy product writing UserId and ProductID");
            System.out.println("4 - Display list of user products by user id");
            System.out.println("5 - Display list of users that bought product by product id ");
            System.out.println("6 - Exit");
            int number = scanner.nextInt();
            int idUser;
            int idProduct;
            User user1 = new User(111, "Oleg", "Tymochko", 10000.0);
            User user2 = new User(112, "Olga", "Tymchak", 12000.0);
            User user3 = new User(113, "Oleksandr", "Tymcoshyk", 0.0);
            User[] users = {user1, user2, user3};

            Product product1 = new Product(911, "Ice-cream caffe", 7500);
            Product product2 = new Product(912, "Dress", 5000);
            Product product3 = new Product(913, "Dodge car 2015", 50000);
            Product[] products = {product1, product2, product3};

            switch (number) {
                case 1:
                    User.displayListUsers(user1, user2, user3);
                    break;
                case 2:
                    Product.displayListProducts(product1, product2, product3);
                    break;
                case 3:
                    System.out.println("Enter Id of user who want to buy productId of\n" +
                            "product which user want to buy");
                    idUser = scanner.nextInt();
                    idProduct = scanner.nextInt();
                    buyProduct(users, products, idUser, idProduct);
                    break;
                case 4:
                    //Display list of user products by user id (If user didn't buy anything yet, don't show anything);
                    break;
                case 5:
                    break;
                //Display list of users that bought product by product id (If nobody bought this product yet, don't show anything)
                case 6:
                    isQuit = true;
                    break;
                default:
                    System.out.println("This variant menu doesn't exist, try again");
                    break;
            }
        }
    }

    public static void buyProduct(User[] users, Product[] products, int idUser, int idProduct) {
        int i = 0, k = 0;
        for (User us : users) {
            i++;
            if (us.getIdUser() == idUser) {
                k++;
                for (Product pr : products) {
                    if (pr.getIdProduct() == idProduct) {
                        if (us.getAmountOfMoney() < pr.getPriceProduct())
                            System.out.println("User can not buy this product, because user has less amount of money than the product's price");
                        else if (us.getAmountOfMoney() > pr.getPriceProduct()) {
                            System.out.println("User  bought a product");
                            users[i - 1].setAmountOfMoney(users[i - 1].getAmountOfMoney() - products[k - 1].getPriceProduct());
                            i = i - 1;
                            k = k - 1;
                            System.out.println("User #" +  users[i].getIdUser() + " successfully bought " + "product with number #" + products[k].getIdProduct() );
                        }
                    }
                }
            }
        }
        // return users[i] & products[k] - I don't know which collection will be the best for tis case
    }

}
